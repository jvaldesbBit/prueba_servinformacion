# usuarios/views_api.py

# Importaciones de Django
from django.shortcuts import get_object_or_404

# Importaciones de REST
from rest_framework import viewsets
from rest_framework.response import Response

# Importaciones propias
from . import models
from . import serializer

class Usuarios_ViewSet (viewsets.ModelViewSet):

    queryset = models.Usuario.objects.all()
    serializer_class = serializer.Usuario_Serializer


class ListViewSet(viewsets.ViewSet):
    
    def list(self, request):
        queryset = models.Usuario.objects.all()
        serializer_ = serializer.Usuario_Serializer(queryset, many=True)
        return Response(serializer_.data)

    def retrieve(self, request, pk=None):
        queryset = models.Usuario.objects.all()
        user = get_object_or_404(queryset, pk=pk)
        serializer_pk = serializer.Usuario_Serializer(user)
        return Response(serializer_pk.data)

    def post(self, request, format=None):
        serializer_crea = serializer.Usuario_Serializer(data=request.data)
        if serializer_crea.is_valid():
            serializer_crea.save()
            return Response(serializer_crea.data, status=status.HTTP_201_CREATED)
        return Response(serializer_crea.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, pk, format=None):
        usuario = self.get_object(pk)
        serializer_put = serializer.Usuario_Serializer(usuario, data=request.data)
        if serializer_put.is_valid():
            serializer_put.save()
            return Response(serializer_put.data)
        return Response(serializer_put.errors, status=status.HTTP_400_BAD_REQUEST)