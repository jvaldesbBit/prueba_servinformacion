# usuarios/url_api.py

# Importaciones de Django

# Importaciones de REST
from rest_framework import routers

# Importaciones propias
from usuarios import views_api

router = routers.DefaultRouter()
router.register(r'api', views_api.Usuarios_ViewSet)
router.register(r'usuario/lista', views_api.ListViewSet, basename='Usuario')