# usuarios/admin.py

# Importaciones de Django
from django.contrib import admin

# Importaciones propias
from .models import Usuario

admin.site.register(Usuario)