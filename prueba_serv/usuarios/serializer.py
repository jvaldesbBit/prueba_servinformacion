# usuarios/serializer.py

# Importaciones de Django

# Importaciones de REST
from rest_framework import serializers

# Importaciones propias
from . import models

class Usuario_Serializer (serializers.ModelSerializer):
    
    class Meta:
        model = models.Usuario
        fields = ("id", "nombre", "apellido", "direccion", "ciudad", "longitud", "latitud", "estadogeo")

        
