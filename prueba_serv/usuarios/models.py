# usuarios/models.py

# Importaciones de Django
from django.db import models

class Usuario(models.Model):
    id = models.CharField(max_length=10, primary_key=True,  verbose_name='Id')
    nombre = models.CharField(verbose_name='Nombre (s)', max_length=60, null=True)
    apellido = models.CharField(verbose_name='Apellido (s)', max_length=150, null=True)
    direccion = models.CharField(verbose_name='dirección', max_length=200, null=True)
    ciudad = models.CharField(verbose_name='Ciudad', max_length=200, null=True)
    longitud = models.CharField(verbose_name='Longitud', max_length=200, null=True)
    latitud = models.CharField(verbose_name='Latitud', max_length=200, null=True)
    estadogeo = models.CharField(verbose_name='Estadogeo', max_length=200, null=True)

    class Meta:
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'

    def __str__(self):
        return self.id+" "+self.nombre+" "+self.apellido